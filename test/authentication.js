let chai = require('chai');
let expect = require('chai').expect;
let chaiHttp = require('chai-http');
let server = require('../app.js');

chai.use(chaiHttp);



let userTestId = ''




/* asyn test */
describe('#test de autenticacion', () => {
  it('guarda usuario', done => {
      chai.request(server)
          .post('/api/register')
          // .set('content-type', 'application/x-www-form-urlencoded')
          .send({fullName: "user test",password: "user123", email: "user@test.com"})
          .end(function(err, res) {
              expect(res).to.have.status(200);
              userTestId = res.body._id
              done();
          });
  }).timeout(0);

  it('loguear usuario', done => {
      chai.request(server)
          .post('/api/authenticate')
          // .set('content-type', 'application/x-www-form-urlencoded')
          .send({fullName: "user test",password: "user123", email: "user@test.com"})
          .end(function(err, res) {
            expect(res).to.have.status(200);
            done();
          })
  }).timeout(0);

  it('elimina usuario', done => {
      chai.request(server)
          .del('/api/delete')
          // .set('content-type', 'application/x-www-form-urlencoded')
          .send({_id:userTestId})
          .end(function(err, res) {
              expect(res).to.have.status(200);
              done();
          })
  }).timeout(0);
})
