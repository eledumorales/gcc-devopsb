GCC-DEVOPS

----- gitlab-ci.yml -----

El archivo gitlab-ci.yml, es utilizada dentro de la configuración de un repositorio para la integración
contínua del proyecto. Esto tiene como ventaja el poder realizar un seguimiento del código durante el
build, testing y despliegue del proyecto.

En el caso de este proyecto, el gitlab-ci.yml inicialmente especifica con la variable image, el lenguaje
y versión utilizada en el proyecto.
Luego configura tres etapas:
-Testing
-Desarrollo
-Produccion

En la etapa de TESTING, se especifican los comandos necesarios para correr los test dentro del proyecto.
en caso de que la evaluación sea exitosa, se pasa a la siguiente etapa.

En la etapa de DESARROLLO, se especifica como imagen Ruby, y se especifican los comandos necesarios para
el despliegue del proyecto en la aplicación configurada en heroku. La HEROKU_API_KEY, es una variable
almacenada dentro de la configuracion del CI/CD de GitLab.

Una vez que el despliegue de la etapa desarrollo culmine con éxito. Se pasa a la última etapa, la de
PRODUCCION. En ésta se realiza el mismo despliegue configurado en la etapa de DESARROLLO


OBSERVACIONES:
- Se selecciono el lenguaje ruby para el despliegue, porque Heroku facilita el despliegue de proyectos
  con dicho lenguaje.

- La etapa DESARROLLO originalmente se configuro para analizar los cambios generados dentro de una rama
  "developer" y la etapa PRODUCCION para realizar un merge de la rama "developer" con la rama "master" y
  finalmente deployar en la aplicación final de HEROKU. Pero finalmente se optó por desplegar directamente
  el proyecto en caso de que la etapa TESTING sea exitosa. Es por ello que las etapas DESARROLLO y PRODUCCION
  poseen la misma configuración
